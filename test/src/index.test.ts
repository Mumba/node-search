import {SearchService} from '../../src/SearchService';
/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from 'assert';
import {DiContainer} from 'mumba-typedef-dicontainer';
import {registerSearchService} from '../../src/index';

describe('index unit tests', () => {
	it('should register artifacts in the DI container', (done) => {
		let container: DiContainer = new Sandal();

		container.object('elasticsearchOptions', {});
		container.object('searchServiceOptions', {});

		registerSearchService(container);

		container.resolve((err: Error, searchService: SearchService) => {
			if (err) {
				return done(err);
			}

			assert(searchService instanceof SearchService, 'should be a search service');
			done();
		});
	});
});
