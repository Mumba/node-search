/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import * as Elasticsearch from 'elasticsearch';
import {SearchService, SearchServiceOptions} from '../../src/SearchService';

describe('SearchService unit tests', () => {
	let elasticsearchClient: Elasticsearch.Client;
	let options: SearchServiceOptions;
	let instance: SearchService;

	beforeEach(() => {
		elasticsearchClient = Object.create(Elasticsearch.Client.prototype);
		options = {
			index: 'the-index'
		};
		instance = new SearchService(options, elasticsearchClient);
	});

	it('should throw if `elasticsearchClient` missing from constructor', () => {
		assert.throws(() => {
			new SearchService(options, void 0);
		}, /<elasticsearchClient> required/);
	});

	it('should search for resources', () => {
		elasticsearchClient.search = (params: Elasticsearch.SearchParams) => new Promise((resolve) => {
			assert.equal(params.index, 'the-index', 'should be the search index');
			assert.equal(params.type, 'site', 'should be the type filter');
			assert.equal(params.body.query.match._all, 'foo', 'should be search term');

			resolve({
				hits: {
					hits: [
						{
							_index: 'default',
							_type: 'site',
							_id: 'sitesById.123',
							_score: 1,
							_source: {
								id: 123,
								name: 'test-name',
							}
						}
					]
				}
			});
		});

		return instance.search('foo')
			.then((results: any[]) => {
				assert.equal(results[0]._score, 1, 'should be the score');
				assert.equal(results[0].id, 123, 'should be the result');
			});
	});
});
