# Mumba Search

A service for working with JSON Web Tokens based on [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken).

## Installation

```sh
$ npm install --save mumba-search
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba WAMP_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-wamp/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2017 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

