/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const elasticsearch = require('elasticsearch');
import {DiContainer} from 'mumba-typedef-dicontainer';
import {SearchService} from './SearchService';

export * from './SearchService';

/**
 * Register SearchService in a DI container.
 *
 * @param {DiContainer} container
 */
export function registerSearchService(container: DiContainer) {
	container.factory('elasticsearchClient', (elasticsearchOptions: Elasticsearch.ConfigOptions) => {
		return new elasticsearch.Client(elasticsearchOptions);
	});

	container.service('searchService', SearchService)
}
