/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as Elasticsearch from 'elasticsearch';
import SearchResponse = Elasticsearch.SearchResponse;

export interface SearchServiceOptions {
	index: string;
}

/**
 * Service to interface with the search server.
 */
export class SearchService {
	/**
	 * Constructor.
	 *
	 * @param {SearchServiceOptions} searchServiceOptions
	 * @param {Elasticsearch.Client} elasticsearchClient
	 */
	constructor(private searchServiceOptions: SearchServiceOptions, private elasticsearchClient: Elasticsearch.Client) {
		if (!elasticsearchClient) {
			throw new Error('SearchService: <elasticsearchClient> required.');
		}
	}

	/**
	 * Perform a basic search.
	 *
	 * @param {string} query
	 * @returns {Promise<*[]>}
	 */
	public search(query: string): Promise<any[]> {
		// TODO - See if this hack can be removed one day. Fix for weird compile error in TS v2.1.5,2.2.0.
		return (<Promise<any>>this.elasticsearchClient.search({
			index: this.searchServiceOptions.index,
			type: 'site',
			body: {
				query: {
					match: {
						_all: query
					}
				}
			}
		}))
			.then((resp: any) => {
				return resp.hits.hits.map((hit: any) => Object.assign({
					_score: hit._score
				}, hit._source,));
			});
	}
}
